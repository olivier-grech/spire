# Foundry Virtual Tabletop Spire System

This is a package for [Foundry Virtual Tabletop](https://foundryvtt.com/) allowing you to play the role-playing game Spire, published by [Rowan, Rook and Decard](https://rowanrookanddecard.com/).

The package includes a character sheet as well as an automated roll system.

Background image used with the authorization of Rowan, Rook and Decard.

You can support me if you like my work:

[![Buy Me a Coffee](./bmc-button.svg)](https://www.buymeacoffee.com/oliviergrech)

## Screenshots

![Screenshot](screenshots/screenshot.jpg)

## Translation

The following languages are available at the moment:

- German
- English
- Spanish
- French
- Italian

All are based on official translations.

## Contributors

Thanks to:

- Fenris1492 for providing the German translation
- Juan Estévez for providing the Spanish translation
- Arcadio21 for providing the Italian translation
