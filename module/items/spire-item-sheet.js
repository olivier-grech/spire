export class SpireItemSheet extends ItemSheet {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["spire", "sheet", "item"],
    });
  }

  get template() {
    return `systems/spire/templates/items/spire-${this.item.type}-sheet.html`;
  }

  getData() {
    const data = super.getData();
    const itemData = data.item;
    data.system = itemData.system;
    return data;
  }
}
