import { SpireItemSheet } from "./items/spire-item-sheet.js";
import { SpireCharacterSheet } from "./actors/spire-character-sheet.js";
import { SpireActor } from "./spire-actor.js";

Hooks.once("init", async function () {
  console.log(`Initializing Spire system`);

  CONFIG.Actor.documentClass = SpireActor;

  // Register actors sheets
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("spire", SpireCharacterSheet, {
    types: ["character"],
    makeDefault: true,
  });

  // Register items sheets
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("spire", SpireItemSheet, {
    types: ["ability"],
    makeDefault: true,
  });
  Items.registerSheet("spire", SpireItemSheet, {
    types: ["bond"],
    makeDefault: true,
  });
  Items.registerSheet("spire", SpireItemSheet, {
    types: ["equipment"],
    makeDefault: true,
  });
  Items.registerSheet("spire", SpireItemSheet, {
    types: ["fallout"],
    makeDefault: true,
  });
  Items.registerSheet("spire", SpireItemSheet, {
    types: ["knack"],
    makeDefault: true,
  });
  Items.registerSheet("spire", SpireItemSheet, {
    types: ["refresh"],
    makeDefault: true,
  });

  // Pre-load templates
  const templatePaths = [
    "systems/spire/templates/actors/spire-character-sheet.html",
    "systems/spire/templates/items/spire-ability-sheet.html",
    "systems/spire/templates/items/spire-bond-sheet.html",
    "systems/spire/templates/items/spire-equipment-sheet.html",
    "systems/spire/templates/items/spire-fallout-sheet.html",
    "systems/spire/templates/items/spire-knack-sheet.html",
    "systems/spire/templates/items/spire-refresh-sheet.html",
  ];
  loadTemplates(templatePaths);
});
